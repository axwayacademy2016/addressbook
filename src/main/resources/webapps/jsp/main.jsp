<!DOCTYPE html>

<html>

 <head><title>JSP sample</title></head>

<body>
<%@ include file="header.jsp" %>

<p>This is my first jsp file.</p>

<%@ page import="java.io.*,java.util.*" %>

<%
   Date sessionCreationDate = new Date(session.getCreationTime());
   Date lastAccessTime = new Date(session.getLastAccessedTime());
   String sessionId = session.getId();
   String context = session.getServletContext().getContextPath();
%>

<p>
    Session create date: <% out.println(sessionCreationDate);%><br/>
    last access time: <% out.println(lastAccessTime); %><br/>
    session id: <% out.println(sessionId); %> <br/>
    context: <% out.println(context); %> <br/>
</p>

<%@ include file="footer.jsp" %>
</body>

</html>