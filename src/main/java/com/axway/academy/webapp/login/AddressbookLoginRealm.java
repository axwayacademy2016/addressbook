package com.axway.academy.webapp.login;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.apache.catalina.realm.GenericPrincipal;
import org.apache.catalina.realm.RealmBase;

public class AddressbookLoginRealm extends RealmBase {

    private static final String REALM_NAME = "AddressbookLoginRealm";
    
    private String mPassword = "123";
    
    @Override
    protected String getName() {
        return REALM_NAME;
    }

    @Override
    protected String getPassword(String username) {
        return mPassword;
    }

    @Override
    protected Principal getPrincipal(String username) {
        List<String> roles = new ArrayList<>();
        roles.add("paljak");
        Principal principal = new GenericPrincipal(username, mPassword, roles);
        return principal;
    }
    
    @Override
    public Principal authenticate(String username, String credentials) {
        return super.authenticate(username, credentials);
    }
    
}
